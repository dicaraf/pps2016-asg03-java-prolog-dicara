%suit(symbol, render)
suit(b, "Bastoni").
suit(d, "Denari").
suit(c, "Coppe").
suit(s, "Spade").
%card_valure(symbol, value, name)
card_value(9, 11, "A").
card_value(0, 0, "2").
card_value(8, 10, "3").
card_value(1, 0, "4").
card_value(2, 0, "5").
card_value(3, 0, "6").
card_value(4, 0, "7").
card_value(5, 2, "J").
card_value(6, 3, "Q").
card_value(7, 4, "K").

%card(-card_value,-suit)
card(X,Y) :- card_value(X,_,_), suit(Y,_).

%get_card_name(+Card,-Name)
get_card_name(card(A,B),N) :- card_value(A,_,N).

%get_card_suit(+Card,-Suit)
get_card_suit(card(A,B),N) :- suit(B,N).

%create_deck(-L): return the ordered list of all combination beetwen card_value and suit
create_deck(L) :- findall(card(X,Y),card(X,Y),L).

%position(+List,+Index,-Elem): return the Elem in Index position in the List
position([H|_],0, H).
position([_|T],N,E) :- N>0, N2 is N-1, position(T,N2,E).

%get_length(+List,-N): return how many elements are in List
get_length([],0).
get_length([_|T],N) :- get_length(T,N2), N is N2+1.

%new_list(+Elem,+List,-NewList): return a new list with Elem deleted from List
new_list(X, [X|T],T).
new_list(X,[Y|T],[Y|T1]) :- new_list(X,T,T1).

%shuffle(+N, +List, -ShuffledList): return a random ShuffledList from a given List
shuffle(N, L1, [C|L2]) :- N>0, N2 is N-1, rand_int(N, X), position(L1,X,C), new_list(C,L1,Lnew), shuffle(N2,Lnew,L2).
shuffle(0, [], []).

%give_card(+List, -Elem, -List): return the first Elem of a given list and return the new list without Elem
give_card([X],X,Lnew).
give_card([X|T], X, Lnew) :- new_list(X,[X|T],Lnew).

%briscola(+List, -Briscola): return the Briscola card as the last element of a given List 
last([X], X).
last([_|T],B) :- last(T,B),!.
briscola(L,B) :- last(L,B).

%add_to_list(+Elem,+List,-NewList): add Elem to List and return NewList
add_to_list(X,L,[X|L]).

%player(name, List)
player(p1,L,L2).
player(p2,L,L2).

% other_player(?Player,?OtherPlayer)
other_player(p1,p2).
other_player(p2,p1).

%compare_cards(+Card, +card, +Briscola, +CurrentPlayer, -Winner): compare two cards and return the winner
compare_cards(card(A,B), card(C,D), card(_,SB), P, Winner) :- B==D, compare_same_suit(card(A,B), card(C,D), P, Winner);
                                                        SB==D, other_player(P,X), Winner=X; Winner=P.
compare_same_suit(card(A,B), card(C,D), P, Winner) :- A>C-> Winner=P; other_player(P,X), Winner=X.

%points(+List,-Points): given a list of cards compute the value
points([card(X,Y)],Points) :- card_value(X,Points,Z).
points([card(X,Y)|T],Points) :- points(T,Partial), card_value(X,P,Z), Points is Partial+P.

%result(+Id,-String)
result(2,"even!").
result(0,"player 1 wins").
result(1,"player 2 wins").

%winner(+Points,+Points,-Winner):given two score return the winner
winner(P1,P2,Winner) :- P1>P2, Winner is 0; P1==P2, Winner is 2; Winner is 1.