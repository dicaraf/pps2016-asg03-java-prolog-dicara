package u07asg

import u07asg.Scala2P.{solveWithSuccess, _}
import alice.tuprolog.{Struct, Term, Theory}
import java.io.FileInputStream
import java.util.Optional

import scala.collection.mutable.Buffer
import collection.JavaConverters._

/**
  * Created by dicaraf on 27/04/2017.
  */
class BriscolaImpl(fileName: String) extends Briscola{
  override def playerToString(player: PlayerB): String = player match {
    case PlayerB.Player1 => "p1"
    case _ => "p2"
  }
  override def stringToPlayer(s: String): PlayerB = s match {
    case "p1" => PlayerB.Player1
    case _ => PlayerB.Player2
  }

  private val engine = mkPrologEngine(new Theory(new FileInputStream(fileName)))

  createShuffledDeck()
  createBriscola()
  createPlayers()
  createTable()

  override def createShuffledDeck(): Unit = {
    val goal = "retractall(deck(_)),create_deck(D),shuffle(40,D,SD),assert(deck(SD))"
    solveWithSuccess(engine,goal)
  }

  override def getDeck = {
    val term = solveOneAndGetTerm(engine, "deck(SD)", "SD").asInstanceOf[Struct]
    val iterator = term.listIterator()
    iterator.asScala.toList.map(_.toString).map{
      case "null" => Optional.empty()
      case s => s
    }.to[Buffer].asJava
  }

  override def createBriscola(): Unit = {
    val goal="retractall(br(_)),deck(SD),briscola(SD, B),assert(br(B))"
    solveWithSuccess(engine, goal)
  }

  override def getBriscola: Term = {
    solveOneAndGetTerm(engine, "br(B)", "B")
  }

  override def createTable(): Unit = {
    val goal="retractall(table(_)),assert(table([]))"
    solveWithSuccess(engine, goal)
  }

  override def getTable: Struct = {
    solveOneAndGetTerm(engine, "table(T)", "T").asInstanceOf[Struct]
  }

  override def giveCard(): Term = {
    if(solveOneAndGetTerm(engine,"deck(S),get_length(S,N)","N").toString.toInt==0){
      val card = null
      card
    } else {
      val card = solveOneAndGetTerm(engine, "deck([X|L])", "X")
      val goal="deck(SD),give_card(SD, X, Lnew)"
      val newSD = solveOneAndGetTerm(engine, goal, "Lnew")
      val goal2=s"retractall(deck(_)),assert(deck(${newSD}))"
      solveWithSuccess(engine,goal2)
      card
    }
  }

  override def createPlayers(): Unit = {
    val goal=s"retractall(player1(_)),assert(player1(player(p1,[${giveCard()},${giveCard()},${giveCard()}],[])))"
    solveWithSuccess(engine, goal)
    val goal2=s"retractall(player2(_)),assert(player2(player(p2,[${giveCard()},${giveCard()},${giveCard()}],[])))"
    solveWithSuccess(engine, goal2)
  }

  override def getPlayer(s: String): Term = s match{
    case "p1" => solveOneAndGetTerm(engine, "player1(P1)", "P1")
    case _ => solveOneAndGetTerm(engine, "player2(P2)", "P2")
  }

  override def getOtherPlayer(s: String): String = s match {
    case "p1" => "p2"
    case _ => "p1"
  }

  override def getPlayerHand(player: String): Struct = player match {
    case "p1" => solveOneAndGetTerm(engine,"player1(player(p1,L,_))", "L").asInstanceOf[Struct]
    case _ => solveOneAndGetTerm(engine,"player2(player(p2,L,_))", "L").asInstanceOf[Struct]
  }

  override def getPlayerCards(player: String): Struct = player match {
    case "p1" => solveOneAndGetTerm(engine,"player1(player(p1,_,L))", "L").asInstanceOf[Struct]
    case _ => solveOneAndGetTerm(engine,"player2(player(p2,_,L))", "L").asInstanceOf[Struct]
  }

  override def compareCards(currentPlayer: String): String = {
    val card1 = solveOneAndGetTerm(engine,"table(T),position(T,0,C1)","C1")
    val card2 = solveOneAndGetTerm(engine,"table(T),position(T,1,C2)","C2")
    val goal = s"compare_cards(${card1}, ${card2}, ${getBriscola()}, ${currentPlayer}, Winner)"
    val winner = solveOneAndGetTerm(engine,goal,"Winner").toString
    createPlayersAfterHand(winner, card1,card2)
    createTable()
    winner
  }

  override def playCard(player: String, cardPos: Int): Unit = player match {
    case "p1" =>
      val playedCard = solveOneAndGetTerm(engine,s"player1(player(p1,L,_)),position(L,${cardPos},Card)", "Card")
      val newCardsHand = solveOneAndGetTerm(engine,s"new_list(${playedCard},${getPlayerHand(player)},New)", "New")
      val goal=s"retractall(player1(_)),assert(player1(player(p1,${newCardsHand},${getPlayerCards(player)})))"
      solveWithSuccess(engine, goal)
      val newT = solveOneAndGetTerm(engine,s"table(T),append(T,[${playedCard}],NewT)", "NewT")
      solveWithSuccess(engine,s"retractall(table(_)),assert(table(${newT}))")
    case _ =>
      val playedCard = solveOneAndGetTerm(engine,s"player2(player(p2,L,_)),position(L,${cardPos},Card)", "Card")
      val newCardsHand = solveOneAndGetTerm(engine,s"new_list(${playedCard},${getPlayerHand(player)},New)", "New")
      val goal=s"retractall(player2(_)),assert(player2(player(p2,${newCardsHand},${getPlayerCards(player)})))"
      solveWithSuccess(engine, goal)
      val newT = solveOneAndGetTerm(engine,s"table(T),append(T,[${playedCard}],NewT)", "NewT")
      solveWithSuccess(engine,s"retractall(table(_)),assert(table(${newT}))")
  }

  override def createPlayersAfterHand(winner: String, card1: Term, card2:Term): Unit = winner match {
    case "p1" =>
      val newCardsList = solveOneAndGetTerm(engine,s"append(${getPlayerCards("p1")},[${card1},${card2}],List)", "List")
      val goal=s"retractall(player1(_)),assert(player1(player(p1,[${giveCard()}|${getPlayerHand("p1")}],${newCardsList})))"
      solveWithSuccess(engine, goal)
      val goal2=s"retractall(player2(_)),assert(player2(player(p2,[${giveCard()}|${getPlayerHand("p2")}],${getPlayerCards("p2")})))"
      solveWithSuccess(engine, goal2)
    case _ =>
      val newCardsList = solveOneAndGetTerm(engine,s"append(${getPlayerCards("p2")},[${card1},${card2}],List)", "List")
      val goal=s"retractall(player2(_)),assert(player2(player(p2,[${giveCard()}|${getPlayerHand("p2")}],${newCardsList})))"
      solveWithSuccess(engine, goal)
      val goal2=s"retractall(player1(_)),assert(player1(player(p1,[${giveCard()}|${getPlayerHand("p1")}],${getPlayerCards("p1")})))"
      solveWithSuccess(engine, goal2)
  }

  override def getPoints(player: String): Term = {
    solveOneAndGetTerm(engine, s"points(${getPlayerCards(player)},Points)", "Points")
  }

  override def getWinner: String = {
    val points1 = getPoints("p1")
    val points2 = getPoints("p2")
    val term = solveOneAndGetTerm(engine, s"winner(${points1},${points2},Winner)", "Winner")
    solveOneAndGetTerm(engine, s"result(${term}, Result)", "Result").toString
  }

  override def getCardFromList(list: Struct, i: Int): String = {
    val card = solveOneAndGetTerm(engine, s"position(${list},${i},C)", "C")
    cardToString(card)
  }

  override def cardToString(card: Term): String = {
    val name = solveOneAndGetTerm(engine, s"get_card_name(${card},N)", "N").toString
    val suit = solveOneAndGetTerm(engine, s"get_card_suit(${card},N)", "N").toString
    name+" di "+suit
  }


}

