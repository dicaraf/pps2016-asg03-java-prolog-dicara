package u07asg;

import alice.tuprolog.Struct;

import javax.swing.*;
import java.awt.*;

/**
 * Created by dicaraf on 29/04/2017.
 */
public class BriscolaApp extends JFrame{

    private final Briscola briscola;
    private final JButton[] p1cards = new JButton[3];
    private final JButton[] p2cards = new JButton[3];
    private final JButton startButton = new JButton("Start");
    private int cardPlayed = 0;
    private int totcardPlayed = 0;
    private PlayerB turn = PlayerB.Player1;
    private PlayerB handWinner = PlayerB.Player1;
    private final JLabel p1Name = new JLabel("Player1: ");
    private final JLabel current = new JLabel("Turn: ");
    private final JLabel briscolaLabel = new JLabel("Briscola: ");
    private final JLabel card1 = new JLabel("");
    private final JLabel deck = new JLabel("");
    private final JLabel p2Name = new JLabel("Player2:");

    private void changeTurn(){
        this.turn = this.turn == PlayerB.Player1 ? PlayerB.Player2 : PlayerB.Player1;
    }

    public BriscolaApp(Briscola briscola) throws Exception {
        this.briscola=briscola;
        initPane();
    }

    private void drawTable() {
        if(cardPlayed==2){
            cardPlayed=0;
            String winner = briscola.compareCards(briscola.playerToString(handWinner));
            handWinner = briscola.stringToPlayer(winner);
            turn = handWinner;
            System.out.println(handWinner+" won hand number "+totcardPlayed/2);
        }
        Struct p1hand = briscola.getPlayerHand("p1");
        Struct p2hand = briscola.getPlayerHand("p2");
        String briscolaCard = briscola.cardToString(briscola.getBriscola());
        if(turn.equals(PlayerB.Player1)){
            for(int i=0;i<3;i++){
                try{
                    p1cards[i].setText(briscola.getCardFromList(p1hand, i));
                }catch(Exception e){
                    p1cards[i].setText("");
                }
                p1cards[i].setEnabled(true);
                p2cards[i].setEnabled(false);
                p2cards[i].setText("");
            }
        } else {
            for(int i=0;i<3;i++){
                try{
                    p2cards[i].setText(briscola.getCardFromList(p2hand, i));
                }catch(Exception e){
                    p2cards[i].setText("");
                }
                p2cards[i].setEnabled(true);
                p1cards[i].setEnabled(false);
                p1cards[i].setText("");
            }
        }
        briscolaLabel.setText("Briscola: "+briscolaCard);
        current.setText("Turn: " + turn);
        try{
            card1.setText("Card: "+briscola.getCardFromList(briscola.getTable(),0));
        } catch (Exception e){  card1.setText("");      }

        deck.setText("Cards played: "+totcardPlayed);

        if(totcardPlayed==40){
            for(int i=0;i<3;i++){
                p1cards[i].setEnabled(false);
                p2cards[i].setEnabled(false);
            }
            startButton.setText(briscola.getWinner());
        }

    }

    private void initPane() {

        setLayout(new BorderLayout());
        JPanel board = new JPanel(new GridLayout(3,4,1,10));

        board.add(p1Name);
        for(int i=0;i<3;i++){
            p1cards[i] = new JButton("");
            p1cards[i].setBackground(Color.BLUE);
            p1cards[i].setEnabled(false);
            int finalI = i;
            p1cards[i].addActionListener(e->{
                cardPlayed++;
                totcardPlayed++;
                briscola.playCard("p1", finalI);
                changeTurn();
                drawTable();
            });
            board.add(p1cards[i]);
        }
        board.add(current);
        board.add(briscolaLabel);
        board.add(card1);
        board.add(deck);
        board.add(p2Name);
        for(int i=0;i<3;i++){
            p2cards[i] = new JButton("");
            p2cards[i].setBackground(Color.CYAN);
            p2cards[i].setEnabled(false);
            int finalI = i;
            p2cards[i].addActionListener(e->{
                cardPlayed++;
                totcardPlayed++;
                briscola.playCard("p2", finalI);
                changeTurn();
                drawTable();
            });
            board.add(p2cards[i]);
        }
        JPanel controls=new JPanel(new FlowLayout());
        controls.add(startButton);
        startButton.addActionListener(e->{
            startButton.setEnabled(false);
            drawTable();
        });
        add(BorderLayout.CENTER, board);
        add(BorderLayout.NORTH, controls);
        setTitle("Play Briscola");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(600, 200);
        setLocationRelativeTo(null);
        setResizable(false);
        setAlwaysOnTop(true);
        setVisible(true);
    }

    public static void main(String[] args){
        try {
            new BriscolaApp(new BriscolaImpl("src/u07asg/briscola.pl"));
        } catch (Exception e) {
            System.out.println(e+"Problems loading the theory");
        }
    }
}