package u07asg

import alice.tuprolog._

object Scala2P {

  def extractTerm(solveInfo:SolveInfo, i:Integer): Term =
    solveInfo.getSolution.asInstanceOf[Struct].getArg(i).getTerm

  def extractTerm(solveInfo:SolveInfo, s:String): Term =
    solveInfo.getTerm(s)


  implicit def stringToTerm(s: String): Term = Term.createTerm(s)
  implicit def seqToTerm[T](s: Seq[T]): Term = s.mkString("[",",","]")
  implicit def stringToTheory[T](s: String): Theory = new Theory(s)

  def mkPrologEngine(theory: Theory): Term => Stream[SolveInfo] = {
    val engine = new Prolog
    engine.setTheory(theory)

    goal => new Iterable[SolveInfo]{

      override def iterator = new Iterator[SolveInfo]{
        var solution: Option[SolveInfo] = Some(engine.solve(goal))

        override def hasNext = solution.isDefined &&
                              (solution.get.isSuccess || solution.get.hasOpenAlternatives)

        override def next() =
          try solution.get
          finally solution = if (solution.get.hasOpenAlternatives) Some(engine.solveNext()) else None
      }
    }.toStream
  }

  def solveWithSuccess(engine: Term => Stream[SolveInfo], goal: Term): Boolean =
    engine(goal).map(_.isSuccess).headOption == Some(true)

  def solveOneAndGetTerm(engine: Term => Stream[SolveInfo], goal: Term, term: String): Term =
    engine(goal).headOption map (extractTerm(_,term)) get
}


object TryScala2P extends App {

  val briscola = new BriscolaImpl("src/u07asg/briscola.pl")

  briscola.createShuffledDeck()
  println(briscola.getDeck())
  briscola.createBriscola()
  println(briscola.getBriscola())
  println("uso tostring   "+briscola.cardToString(briscola.getBriscola))
  briscola.createPlayers()
  println(briscola.getPlayer("p1"))
  println(briscola.getPlayer("p2"))
  println(briscola.getDeck())
  briscola.createTable()
  println(briscola.getTable)
  var current = "p1"
  var i=0
  for(i<- 1 to 18){
    println(i)
    briscola.playCard("p1",1)
    briscola.playCard("p2",1)
    println("tavolo: "+briscola.getTable)
    current=briscola.compareCards(current)
    println("winner e currentPl: "+current)
    println("p1: "+briscola.getPlayer("p1"))
    println("p2: "+ briscola.getPlayer("p2"))
  }
  briscola.playCard("p1",1)
  briscola.playCard("p2",1)
  println("tavolo: "+briscola.getTable)
  current=briscola.compareCards(current)
  println("winner e currentPl: "+current)
  println("p1: "+briscola.getPlayer("p1"))
  println("p2: "+ briscola.getPlayer("p2"))
  briscola.playCard("p1",2)
  briscola.playCard("p2",2)
  println("tavolo: "+briscola.getTable)
  current=briscola.compareCards(current)
  println("winner e currentPl: "+current)
  println("p1: "+briscola.getPlayer("p1"))
  println("p2: "+ briscola.getPlayer("p2"))
  println(briscola.getWinner)


  /*val ttt = new TicTacToeImpl("src/u07asg/ttt.pl")
  ttt.createBoard()
  println(ttt.getBoard())
  println(ttt.checkCompleted())
  ttt.move(Player.PlayerX,0,0)
  ttt.move(Player.PlayerO,1,1)
  ttt.move(Player.PlayerX,2,2)
  println(ttt.getBoard())
  println(ttt.winCount(Player.PlayerO,Player.PlayerX))
  println(ttt.winCount(Player.PlayerO,Player.PlayerO))*/
}
