package u07asg;

import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import java.util.List;

/**
 * Created by dicaraf on 27/04/2017.
 */
enum PlayerB { Player1, Player2 }

public interface Briscola {

    String playerToString(PlayerB player);

    PlayerB stringToPlayer(String s);

    void createShuffledDeck();

    List<Object> getDeck();

    void createBriscola();

    Term getBriscola();

    void createTable();

    Struct getTable();

    Term giveCard();

    void createPlayers();

    Term getPlayer(String s);

    String getOtherPlayer(String s);

    Struct getPlayerHand(String s);

    Struct getPlayerCards(String s);

    String compareCards(String currentPlayer);

    void playCard(String player, int cardPos);

    void createPlayersAfterHand(String winner, Term card1, Term card2);

    Term getPoints(String player);

    String getWinner();

    String getCardFromList(Struct list, int i);

    String cardToString(Term card);
}
